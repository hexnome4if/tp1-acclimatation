<div class="notebook">

<div class="nb-cell html" name="htm1">
<h1>4-IF-ALIA TP d'introduction à PROLOG</h1>
<h2>M. Kaytoue, S. Calabretto et J.-F. Boulicaut.<br>
INSA LYON - Département Informatique - 2016/2017</h2>
<p>Votre effort d’appropriation des concepts du langage (explication du codage des prédicats, usage
de l’unification, approche rigoureuse dans les tests avec notamment l’étude des différentes
configurations d’appel) est indispensable pour s’engager dans la réalisation d’un projet de
programmation sur les deux séances suivantes. Il est crucial de bien avoir compris les cinq
premiers chapitres du livre support de cours. Il sera aussi important de savoir consulter la 
documentation en ligne de swi-prolog (Built-in Predicates).</p>
</div>

<div class="nb-cell html" name="htm2">
<h3>Exercice 1 : la généalogie dans <i>Game of Thrones</i></h3>
Créer une base de connaissances contenant des personnes, divers attributs de celles-ci (par
exemple le genre) et la définition de certaines relations familiales (par exemple, « parent »). 
On vous propose une généalogie dans une certaine représentation, libre à vous de l'utiliser ou non.
On peut alors étudier le codage de nouvelles relations comme « ancêtre » ou « frère ou soeur » ou
encore « oncle ou tante ». Ce domaine simple est très bien pour découvrir les mécanismes d’aide
à la mise au point (« debug » trace/0, notrace/0 sur le client "lourd".) ainsi qu'à l'écriture d'arbres de preuves.
</div>

<div class="nb-cell program" data-background="true" name="p1">
% Base de faits
% Déroulez pour voir la base de connaisance proposée : l'arbre d'une famille de Game Of Thrones.
male(tytos).
male(tywin).
male(kevan).
male(jaime).
male(tyrion).
male(robert).
male(joffrey).
male(tommen).
male(lancel).
female(dorna).
female(myrcella).
female(cersei).
female(joanna).

% relations
parent(tytos,tywin).
parent(tytos,kevan).
parent(tywin,jaime).
parent(tywin,cersei).
parent(tywin,tyrion).
parent(joanna,jaime).
parent(joanna,cersei).
parent(joanna,tyrion).
parent(cersei,joffrey).
parent(cersei,myrcella).
parent(cersei,tommen).
parent(robert,joffrey).
parent(robert,myrcella).
parent(robert,tommen).
parent(kevan,lancel).
parent(dorna,lancel).
</div>

<div class="nb-cell program" data-background="true" name="p2">
frere(X,Y):-parent(Z,X),parent(Z,Y),male(X),male(Y),X\==Y.
soeur(X,Y):-parent(Z,X),parent(Z,Y),female(X),female(Y),X\==Y.
geschwister(X,Y):-parent(Z,X),parent(Z,Y),X\==Y.
    
pere(X,Y):-parent(X,Y),male(X).
mere(X,Y):-parent(X,Y),female(X).

fils(Y,X):-parent(X,Y),male(Y).
fille(Y,X):-parent(X,Y),female(Y).

%Y est la nièce de X si ... X est frère/soeur avec un Z, et Y est la fille de Z.
niece(Y,X):-geschwister(X,Z),fille(Y,Z).
neuveu(Y,X):-geschwister(X,Z),fils(Y,Z).
%NieceOuNeveu
diagonale(X,Y):-geschwister(X,Z),parent(Z,Y).

oncle(X,Y):- niece(Y,X),male(X).
oncle(X,Y):- neuveu(Y,X),male(X).
tante(X,Y):- niece(Y,X),female(X).
tante(X,Y):- neuveu(Y,X),female(X).

oncle_ou_tante(X,Y):- oncle(X,Y).
oncle_ou_tante(X,Y):- tante(X,Y).

cousin(X,Y):- parent(Z,X),geschwister(Z,W),parent(W,Y).

ancetre(X,Y):-parent(X,Y).
ancetre(X,Y):-parent(Z,Y),ancetre(X,Z).
</div>

<div class="nb-cell query" name="q1">
cousin(X,Y)

</div>

<div class="nb-cell html" name="htm3">
<h3>Exercice 2 : Les listes</h3>
<p>Après avoir revu le premier chapitre qui traite des listes dans le cours, vous pouvez coder les prédicats suivants :</p>
<ul>
  <li><code>member(Elem,List)</code> : vrai si <code>Elem</code> appartient à la liste <code>List</code>.</li>
  <li><code>supprime(Elem, Liste, ListeSansElem)</code> : vrai si <code>ListeSansElem</code> est <code>Liste</code> privée des occurences de <code>Elem</code>.</li>
  <li><code>append(Liste1, Liste2, ConcatenationList1etList2)</code>: concaténation de deux listes.</li>
  <li><code>inv(L1,L2)</code>: vrai si les éléments de la seconde liste sont ceux de la première mais dans l'ordre inverse.</li>
  <li><code>subsAll(Valeur,NouvelleValeur, Liste,NouvelleListe)</code>:
    Vrai si la nouvelle liste est l'ancienne ou chaque <code>Valeur</code> a été substituée par <code>NouvelleValeur</code>.<br>
	Ainsi <code>  subsAll(a,x, [a,b,a,c],R)</code> devrait réussir avec <code> R=[x,b,x,c] </code></li>
</ul>
</div>

<div class="nb-cell program" name="p3">
%X est membre de la liste, si X est en tête de liste.
member(X,[X|_]).
%X est membre de la liste, si X est membre de sa queue.
member(X,[_|L]):-member(X,L).

% ==== SUPPRIME ====
%Condition d'arrêt : X est supprimé de la seconde liste, si la queue de la première liste est égale à la deuxième liste.
suppr(_,[],[]).
%Case 1 : X est présent en tête de la liste 1, on le retire de la la liste 1.
suppr(X,[X|Q],Y):- suppr(X,Q,Y).
%Case 2 : X est absent en tête de liste, dans la liste 1 et la liste 2.
suppr(X,[Y|Q],[Y|T]):- X\==Y ,suppr(X,Q,T).

% ==== APPEND ====
%Condition d'arrêt : la liste 1 et 2 sont vides
append([],[],[]).
%Cas 1 : La tête de la liste 1 est égale à la tête de la liste concat
append([X|Q],Y,[X|T]):-append(Q,Y,T).
%Cas 2 : La tête de la liste 2 est égale à la tête de la liste concat
append(X,[Y|Q],[Y|T]):-append(X,Q,T).

% ==== INV ====
% %Condition d'arrêt : les deux listes sont vides
inverse([],[]).
% On prend la tête de la première liste, on considère ..idem. On descend donc dans l'arbre en profondeur. Une fois que L est vide, on cherche à prouver que Y est vide (voir condition d'arrêt).
% On ajoute à la liste vide le dernier élément rencontré de la première liste, pour constitué le premier élément de la deuxième liste (en paramètre initial).
% On remonte au fur et à mesure en rajoutant la tête qu'on avait retiré au niveau de récursion supérieur. En gros, on inverse la liste en partant du dernier élément.
inverse([H|L],M) :- inverse(L,Y), append(Y,[H],M).

% ==== SUBSALL ====
%subsall(X, NewX, Liste, NewListe).
%Condition d'arrêt : les nouvelles et anciennes listes sont vides
subsall(_,_,[],[]).
%On part des listes complètes, on retire chaque élément. Cas 1 : l'élement de tête est l'élement qu'on remplace, Cas 2: l'élement de tête est un autre élément.
subsall(X,Y,[X|T],[Y|Q]) :- subsall(X,Y,T,Q).
subsall(X,Y,[A|T],[A|Q]) :- A\==X, subsall(X,Y,T,Q).
</div>

<div class="nb-cell query" name="q2">
subsall(a,x, [a,b,a,c],R).
</div>

<div class="nb-cell query" name="q3">
% une requête
</div>

<div class="nb-cell html" name="htm4">
<h3>Exercice 3 : Arithmétique</h3>
<p>On ré-écrira et testera d'abord un prédicat qui vérifie la longueur d'une liste <code>len(Liste,Long)</code> avec par exemple <code>len([a,b,c],3)</code> est vrai.</p>

<p>On souhaite adapter le prédicat <code>member</code> pour permettre un accès par le rang. Ainsi on définit <code>element/3</code> ainsi :</p>
  <ul>
    <li><code>element(3,X,[a,b,c,d]).</code> devrait réussir avec <code>X = c</code>.</li>
    <li><code>element(I,a,[a,b,a]).</code> devrait fournir la réponse <code>I = 1</code> puis <code>I = 3</code>.</li>
</ul>
</div>

<div class="nb-cell program" name="p4">
% ==== Length ====
%Condition d'arrêt : la liste est vide.
%On parcours la liste et enlevons à chaque itération un élément, en incrémentant une variable.
len([],0).
len([X|T],N) :- len(T,Y),N is Y+1.

% ==== Element ====
% 
% 
%X est membre de la liste, si X est en tête de liste.
element(X,[X|_]).
%X est membre de la liste, si X est membre de sa queue.
element(X,[_|L]):-member(X,L).
</div>

<div class="nb-cell query" name="q4">
len([1,2,3],X).
</div>

<div class="nb-cell query" name="q5">
% une requête
</div>

<div class="nb-cell query" name="q6">
% une requête
</div>

<div class="nb-cell html" name="htm5">
<h3>Exercice 4 : les ensembles</h3>On représentera des ensembles (en extension) par des listes (sans répétition) d’objets. Ecrire un
prédicat pour tester si une liste est bien un ensemble (pas de répétition). Ecrire un prédicat qui
produise un ensemble (enlève les doublons). Ainsi <code>list2ens([a,b,c,b,a],E) </code> devrait réussir avec
<code>E=[a,b,c]</code>. Evidemment on s’interdira l’utilisation de <code>sort</code> et de <code>setof</code>.
On peut ensuite décrire l’union, l’intersection, la différence, l’égalité de deux ensembles.
</div>

<div class="nb-cell program" name="p5">
%Entrez votre programme ici.
%
%
%
</div>

<div class="nb-cell query" name="q7">
list2ens([a,b,c,b,a],E).
</div>

<div class="nb-cell query" name="q8">
union([a,a,b], [c,d,d], Union).
</div>

<div class="nb-cell query" name="q9">
% autres requêtes à entrer.
</div>

<div class="nb-cell html" name="htm6">
<h3>Exercice 5 : un jeu de morpion (<code>on the way to the project!</code>)</h3>
<p>Avant de vous lancer dans le projet et de choisir un jeu à deux joueurs tenace, 
il est bon de réfléchir à la structure d'un programme PROLOG pour un jeu simple : le morpion.   Dans l'ordre, on veillera à : 
</p>
<ul>
  <li>La représentation du plateau comme un fait que l'on peut ajouter/retirer de manière dynamique à la base de connaissance<code>assert/1</code> et <code>retract/1</code></li>
  <li>L'affichage du plateau avec <code>writeln/1</code></li>
  <li>Le test de fin de partie qui élit le gagnant (égalité possible) <code>gameover/2</code> </li>
  <li>Le prédicat qui permet de choisir une nouvelle case à jouer dans une grille (l'intelligence artificielle qui pourra utiliser les prédicats <code>random/1</code> et <code>repeat/0</code></li>
  <li>Un prédicat qui considère le plateau courant, le coup choisi par l'intelligence artificielle, et produit une nouveau plateau avec le coup joué.</li>
  <li>Un prédicat <code>play/1</code> qui réalise un tour de jeu pour le joueur donnée en paramètre (fait intervenir tous les autres prédicats).</li>
</ul>
</div>

<div class="nb-cell program" data-background="true" name="p6">
:- dynamic board/1. % permet l'assertion et le retrait de faits board/1

play(Player):-  write('New turn for:'), writeln(Player),
    		board(Board), % instanciate the board from the knowledge base 
       	    displayBoard, % print it
            ia(Board, Move,Player), % ask the AI for a move, that is, an index for the Player 
    	    playMove(Board,Move,NewBoard,Player), % Play the move and get the result in a new Board
		    applyIt(Board, NewBoard), % Remove the old board from the KB and store the new one
    	    changePlayer(Player,NextPlayer), % Change the player before next turn
            play(NextPlayer). % next turn!

%%%%% Start the game! 
init :- length(Board,9), assert(board(Board)), play('x').
</div>

<div class="nb-cell query" data-tabled="true" name="q10">
init.
</div>

<div class="nb-cell program" name="p7">

</div>

</div>
